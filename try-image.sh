#!/usr/bin/env bash

set -e

docker build -t node-build-env .
docker run --rm -it node-build-env bash